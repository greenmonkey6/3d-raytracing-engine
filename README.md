# Ray Tracer

A very simple Ray Tracer using linear algebra as taught in school.

The image is generated by just computing the intersections of planes and rays.

![Imgur](https://i.imgur.com/B8HinPG.png)

## Installation

```sh
$ make
```
