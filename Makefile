MAIN_CLASS = Main

# Goes to the dir of the class files and creates the jar in its dir (in case they are different)
RayTracer.jar: bin/*.class
	mkdir -p $(@D)
	cd $(<D) && \
		jar cfe ../$@ $(MAIN_CLASS) $(^F)

# Compiles all source files from the source dir and puts them in the class file dir
bin/%.class: src/*.java
	mkdir -p bin/
	javac -d bin/ $^

.PHONY: clean

# Removes all build files
clean:
	rm -r bin
	rm *.jar
