/**
 * 
 * @author Tobias Schleifstein
 * 
 */
public class Main
{
	/**
	 * Hier startet das Programm
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		// Erstellt eine Tastatur
		Keyboard keyboard = new Keyboard();
		
		// Erstellt ein Fenster und uebergibt die Tastatur
		Frame frame = new Frame(keyboard);
		
		// Erstellt ein Spiel und uebergibt die Tastatur
		Game game = new Game(keyboard);
		
		// Variablen zur Berechnung der Zeit seit dem letzen Frame
		double delta, lastFrame, fps;
		
		// Setzt die Zeit des letzen Frames auf diesen Moment
		lastFrame = System.currentTimeMillis();
		
		// Startet die Endlosschleife, welche ueber die gesamte Laufzeit die
		// Spielroutine wiederholt
		while (true)
		{
			// Berechnung der Zeit seit dem letzten Frame
			delta = (System.currentTimeMillis() - lastFrame) / 1000d;
			lastFrame = System.currentTimeMillis();
			fps = 1 / delta;
			
			// Setzt die fps in den Fenster Header
			frame.setTitle(String.format("%02f fps", fps));
			
			// Aktualisiert und rendert das Spiel
			game.update(delta);
			game.render(frame.getImage());
			
			// Zeichnet das Fenster neu
			frame.repaint();
		}
	}
}
