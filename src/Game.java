import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;


/**
 * 
 * @author Tobias Schleifstein
 * 
 */
public class Game
{
	// -------Instanzvariablen--------------------------------------------------
	
	/** Position des Beobachters */
	private Point3D pos;
	
	/** Position des Bildes */
	private Point3D imagePos;
	
	/** Erster Richtungsvektor des Bildes */
	private Point3D imageXDirection;
	
	/** Zweiter Richtungsvektor des Bildes */
	private Point3D imageYDirection;
	
	/** Anzuzeigende Elemente */
	private Element[] elements;
	
	/** Schnittstelle zur Tastatur */
	private Keyboard keyboard;
	
	/** Bewegungsgeschwindigkeit (in pxl/s) */
	private double movingSpeed;
	
	// -------Konstruktor-------------------------------------------------------
	
	/**
	 * Erstellt ein Spiel mit standard Positionen von Spieler und Bild, sowie
	 * einige Elemente.
	 * 
	 * @param keyboard
	 *            Schnittstelle zur Tastatur.
	 */
	public Game(Keyboard keyboard)
	{
		// Speichert die Tastatur
		this.keyboard = keyboard;
		
		// Initialisiert die Blickrichtung und Position des Beobachters und des
		// Bildes
		pos = new Point3D(0, 0, 0);
		
		imagePos = new Point3D(0, 0, 500);
		
		imageXDirection = new Point3D(1, 0, 0);
		
		imageXDirection = new Point3D(0, 1, 0);
		
		// Erstellt die Elemente
		elements = new Element[5];
		elements[0] = new Surface(new Point3D(0, 0, 1000),
				new Point3D(1, 0, 0), new Point3D(0, 1, 0), 500, 500,
				Color.RED.getRGB());
		elements[1] = new Surface(new Point3D(0, 0, 800), new Point3D(1, 0, 0),
				new Point3D(0, 1, 0), 50, 50, Color.BLUE.getRGB());
		elements[2] = new Surface(new Point3D(-60, -30, 900), new Point3D(1, 0,
				0), new Point3D(0, 1, 0.5), 200, 200, Color.GREEN.getRGB());
		elements[3] = new Surface(new Point3D(-100, 200, 1100), new Point3D(1,
				0, 2), new Point3D(0, 1, 0), 300, 300, Color.WHITE.getRGB());
		elements[4] = new Light(500000, new Point3D(-200, 0, 500));
		
		// Initialisiert die Bewegungsgeschwindigkeit
		movingSpeed = 500;
	}
	
	// -------Methoden----------------------------------------------------------
	
	/**
	 * Rendert das Spiel auf image.
	 * 
	 * @param image
	 *            Bild auf dem das Spiel gerendert werden soll.
	 */
	public void render(BufferedImage image)
	{
		// Geht alle Pixel der Bildes durch
		for (int i = 0; i < image.getWidth(); i++)
		{
			for (int j = 0; j < image.getHeight(); j++)
			{
				// Erstellt einen Strahl, der beim Beobachter startet und in
				// Richtung des entsprechenden Pixels verlaeuft
				Ray ray = new Ray(pos,
						new Point3D(imagePos.x - image.getWidth() / 2 + i
								- pos.x, imagePos.y - image.getHeight() / 2 + j
								- pos.y, imagePos.z - pos.z));
				
				// Laesst den Strahl mit allen Elementen kollidieren, um
				// herauszufinden, welches Element an dieser Stelle zu sehen ist
				for (int k = 0; k < elements.length; k++)
				{
					if (elements[k] != null)
					{
						elements[k].collide(ray);
					}
				}
				
				// Lichtlevel der aktuellen Stelle (reicht von 0 bis 1)
				double glowLevel = 0;
				
				// Sucht unter allen Elementen alle Instanzen von Light
				for (int k = 0; k < elements.length; k++)
				{
					if (elements[k] != null
							&& ray.getClosestCollision() != null
							&& elements[k] instanceof Light)
					{
						Light light = (Light)elements[k];
						
						// Erstellt einen Schattenstrahl, der von dem
						// Kollisionspunkt des Strahls mit dem Element in
						// Richtung der Lichtquelle verlaeuft
						Ray shadow = new Ray(
								ray.getClosestCollisionPoint(),
								new Point3D(
										light.getPos().x
												- ray.getClosestCollisionPoint().x,
										light.getPos().y
												- ray.getClosestCollisionPoint().y,
										light.getPos().z
												- ray.getClosestCollisionPoint().z));
						
						// Laesst den Schattenstrahl mit allen Elemeten
						// kollidieren, um herauzufinden, ob sich etwas zwischen
						// Kollisionspunkt und Lichtquelle befindet
						for (int l = 0; l < elements.length; l++)
						{
							if (elements[l] != null)
							{
								elements[l].collide(shadow);
							}
						}
						
						// Beleuchtet die Stelle, wenn sich nichts dazwischen
						// befindet
						if (shadow.getClosestCollision() == null
								&& !shadow
										.isClosestCollisionWithinLightDistance())
						{
							double distance = Math.sqrt(Math.pow(
									light.getPos().x - shadow.getStart().x, 2)
									+ Math.pow(
											light.getPos().y
													- shadow.getStart().y, 2)
									+ Math.pow(
											light.getPos().z
													- shadow.getStart().z, 2));
							
							glowLevel = 1 / Math.pow(distance, 2)
									* light.getGlowLevel();
						}
					}
				}
				
				// Setzt Farbe des Pixels entsprechend des Strahls und der
				// Helligkeit
				Color rayColor = new Color(ray.getClosestCollisionColor());
				
				int red = (int)((rayColor.getRed() + 1) * glowLevel);
				int green = (int)((rayColor.getGreen() + 1) * glowLevel);
				int blue = (int)((rayColor.getBlue() + 1) * glowLevel);
				
				if (red > 255)
				{
					red = 255;
				}
				
				if (green > 255)
				{
					green = 255;
				}
				
				if (blue > 255)
				{
					blue = 255;
				}
				
				Color pixelColor = new Color(red, green, blue);
				
				image.setRGB(i, j, pixelColor.getRGB());
			}
		}
	}
	
	/**
	 * Aktualisiert die Position der Elemente.
	 * 
	 * @param delta
	 *            Zeit seit dem letzten Frame.
	 */
	public void update(double delta)
	{
		if (keyboard.isPressed(KeyEvent.VK_W))
		{
			pos.z += delta * movingSpeed;
			imagePos.z += delta * movingSpeed;
		}
		
		if (keyboard.isPressed(KeyEvent.VK_A))
		{
			pos.x -= delta * movingSpeed;
			imagePos.x -= delta * movingSpeed;
		}
		
		if (keyboard.isPressed(KeyEvent.VK_S))
		{
			pos.z -= delta * movingSpeed;
			imagePos.z -= delta * movingSpeed;
		}
		
		if (keyboard.isPressed(KeyEvent.VK_D))
		{
			pos.x += delta * movingSpeed;
			imagePos.x += delta * movingSpeed;
		}
		
		if (keyboard.isPressed(KeyEvent.VK_SPACE))
		{
			pos.y -= delta * movingSpeed;
			imagePos.y -= delta * movingSpeed;
		}
		
		if (keyboard.isPressed(KeyEvent.VK_SHIFT))
		{
			pos.y += delta * movingSpeed;
			imagePos.y += delta * movingSpeed;
		}
		
		if (keyboard.isPressed(KeyEvent.VK_UP))
		{
			System.out.println("Hoch");
		}
		
		if (keyboard.isPressed(KeyEvent.VK_LEFT))
		{
			((Light)elements[4]).addX(-delta * movingSpeed);
		}
		
		if (keyboard.isPressed(KeyEvent.VK_DOWN))
		{
			System.out.println("Runter");
		}
		
		if (keyboard.isPressed(KeyEvent.VK_RIGHT))
		{
			((Light)elements[4]).addX(delta * movingSpeed);
		}
		
		if (keyboard.isPressed(KeyEvent.VK_ESCAPE))
		{
			System.exit(0);
		}
	}
}
