import java.awt.Color;
import java.util.ArrayList;


/**
 * 
 * @author Tobias Schleifstein
 * 
 */
public class Ray
{
	// -------Instanzvariablen--------------------------------------------------
	
	/** Hintergrundfarbe */
	public static final int BACKGROUND_COLOR;
	
	static
	{
		BACKGROUND_COLOR = Color.LIGHT_GRAY.getRGB();
	}
	
	/** Liste der Kollisionen */
	private ArrayList<Collision> collisions;
	
	/** Ortsvektor */
	private Point3D start;
	
	/** Richtungsvektor */
	private Point3D direction;
	
	// -------Konstruktor-------------------------------------------------------
	
	/**
	 * Erstellt einen Strahl mit einem Ortsvektor und einem Richtungsvektor
	 * 
	 * @param start
	 *            Ortsvektor des Strahls.
	 * @param direction
	 *            Richtungsvektor des Strahls.
	 */
	public Ray(Point3D start, Point3D direction)
	{
		// Ortsvektor speichern
		this.start = start;
		
		// Richtungsvektor speichern
		this.direction = direction;
		
		// Kollisionen initialisieren
		collisions = new ArrayList<Collision>();
	}
	
	// -------Methoden----------------------------------------------------------
	
	/**
	 * Gibt die Distanz zur naechsten Kollision an
	 * 
	 * @return Distanz zur naechsten Kollision
	 */
	public double getClosestCollisionDistance()
	{
		Collision collision = getClosestCollision();
		
		if (collision == null)
		{
			return 0;
		}
		else
		{
			return collision.getDistance();
		}
	}
	
	/**
	 * Gibt die Farbe der naechsten Kollision an
	 * 
	 * @return Farbe der naechsten Kollision
	 */
	public int getClosestCollisionColor()
	{
		Collision collision = getClosestCollision();
		
		if (collision == null)
		{
			return BACKGROUND_COLOR;
		}
		else
		{
			return collision.getColor();
		}
	}
	
	/**
	 * Gibt den naechsten Kollisionspunkt zurueck.
	 * 
	 * @return naechster Kollisionspunkt.
	 */
	public Point3D getClosestCollisionPoint()
	{
		Collision collision = getClosestCollision();
		
		if (collision == null)
		{
			return null;
		}
		else
		{
			return collision.getCollisionPoint();
		}
	}
	
	/**
	 * Gibt zurueck, ob sich die naechste Kollision im Richtungsvektor befindet
	 * 
	 * @return ob sich die naechste Kollision im Richtungsvektor befindet
	 */
	public boolean isClosestCollisionWithinLightDistance()
	{
		Collision collision = getClosestCollision();
		
		if (collision == null)
		{
			return false;
		}
		else
		{
			return collision.isWithinLightDistance();
		}
	}
	
	/**
	 * Gibt die Entfernung zur naechsten Kollision an
	 * 
	 * @return Entfernung zur naechsten Kollision
	 */
	public Collision getClosestCollision()
	{
		if (collisions.size() == 0)
		{
			return null;
		}
		else
		{
			Collision closest = null;
			
			for (int i = 0; i < collisions.size(); i++)
			{
				if (closest == null
						|| collisions.get(i).getDistance() < closest
								.getDistance())
				{
					closest = collisions.get(i);
				}
			}
			
			return closest;
		}
	}
	
	/**
	 * Fuegt eine neue Kollsion an der entsprechenden Stelle hinzu.
	 * 
	 * @param collision
	 *            Entfernung zur Kollision.
	 * @param color
	 *            Farbe des Kollisionspunktes.
	 */
	public void addCollision(double distance, int color,
			Point3D collisionPoint, boolean withinLightDistance)
	{
		// Laesst die Kollision nur zu, wenn die Distanz groesser als 1 ist, um
		// Fehler aufgrund von Rechenungenauigkeiten zu vermeiden
		if (distance > 1)
		{
			collisions.add(new Collision(distance, color, collisionPoint,
					withinLightDistance));
		}
	}
	
	/**
	 * Gibt den Ortsvektor des Strahls zurueck.
	 * 
	 * @return Ortsvektor des Strahls.
	 */
	public Point3D getStart()
	{
		return start;
	}
	
	/**
	 * Gibt den Richtungsvektor des Strahls zurueck.
	 * 
	 * @return Richtungsvektor des Strahls.
	 */
	public Point3D getDirection()
	{
		return direction;
	}
	
	@Override
	public String toString()
	{
		return String.format("Ortsvektor(%f %f %f) Richtungsvektor(%f %f %f)",
				start.x, start.y, start.z, direction.x, direction.y,
				direction.z);
	}
	
	// -------Private Klassen---------------------------------------------------
	
	private class Collision
	{
		// -------Instanzvariablen----------------------------------------------
		
		/** Entfernung zur Kollision, welche dem Strahl seine Farbe gegeben hat. */
		private double distance;
		
		/** Farbe, an der Stelle der Kollision. */
		private int color;
		
		/** Kollisionspunkt. */
		private Point3D collisionPoint;
		
		/** Gibt an, ob sich die Kollision im Richtungsvektor befindet. */
		private boolean withinLightDistance;
		
		// -------Konstruktor---------------------------------------------------
		
		public Collision(double distance, int color, Point3D collisionPoint,
				boolean withinLightDistance)
		{
			// Daten speichern
			this.distance = distance;
			this.color = color;
			this.collisionPoint = collisionPoint;
			this.withinLightDistance = withinLightDistance;
		}
		
		// -------Methoden------------------------------------------------------
		
		/**
		 * Gibt die Distanz zur Kollision zurueck.
		 * 
		 * @return Distanz zur Kollision.
		 */
		public double getDistance()
		{
			return distance;
		}
		
		/**
		 * Gibt die Farbe des Kollisionspunktes zurueck.
		 * 
		 * @return Farbe des Kollisionspunktes.
		 */
		public int getColor()
		{
			return color;
		}
		
		/**
		 * Gibt den Kollisionspunkt zurueck.
		 * 
		 * @return x-Koordinate des Kollisionspunktes.
		 */
		public Point3D getCollisionPoint()
		{
			return collisionPoint;
		}
		
		/**
		 * Gibt zurueck, ob sich die Kollision im Richtungsvektor befindet.
		 * 
		 * @return ob sich die Kollision im Richtungsvektor befindet.
		 */
		public boolean isWithinLightDistance()
		{
			return withinLightDistance;
		}
	}
}
