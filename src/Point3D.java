public class Point3D
{
	/**
	 * Koordinaten des Punkts.
	 */
	public double x, y, z;
	
	// -------Konstruktoren-----------------------------------------------------
	
	/**
	 * Erstellt einen Punkt mit 3 Koordinaten.
	 * 
	 * @param x
	 *            x-Koordinate des Punkts.
	 * @param y
	 *            y-Koordinate des Punkts.
	 * @param z
	 *            z-Koordinate des Punkts.
	 */
	public Point3D(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
}
