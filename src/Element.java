/**
 * 
 * @author Tobias Schleifstein
 * 
 */
public abstract class Element
{
	// -------Konstruktor-------------------------------------------------------
	
	/**
	 * Erstellt ein Element
	 */
	public Element()
	{
		super();
	}
	
	// -------Methoden----------------------------------------------------------
	
	/**
	 * Laesst einen Strahl mit dem Element kollidieren
	 * 
	 * @param ray
	 *            Strahl, der mit dem Element kollidiert
	 */
	public abstract void collide(Ray ray);
}
