public class Light extends Element
{
	// -------Instanzvariablen--------------------------------------------------
	
	/**
	 * Leuchtstaerke des Lichtes
	 */
	private double glowLevel;
	
	/**
	 * Koordinate des Lichtes
	 */
	private Point3D pos;
	
	// -------Konstruktor-------------------------------------------------------
	
	public Light(double glowLevel, Point3D pos)
	{
		this.glowLevel = glowLevel;
		
		// Koordinaten speichern
		this.pos = pos;
	}
	
	// -------Methoden----------------------------------------------------------
	
	@Override
	public void collide(Ray ray)
	{
		// wird nicht benoetigt (bzw. ist aufgrund von Punktkollision nicht
		// moeglich)
	}
	
	/**
	 * Gibt das Leuchtlevel des Lichtes zurueck
	 * 
	 * @return Leuchtlevel des Lichtes
	 */
	public double getGlowLevel()
	{
		return glowLevel;
	}
	
	/**
	 * Gibt die Position des Lichtes zurueck.
	 * 
	 * @return Position des Lichtes.
	 */
	public Point3D getPos()
	{
		return pos;
	}
	
	public void addX(double value)
	{
		pos.x += value;
	}
}
