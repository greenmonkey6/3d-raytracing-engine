import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


/**
 * 
 * @author Tobias Schleifstein
 * 
 */
public class Keyboard implements KeyListener
{
	// -----Instanzvariablen----------------------------------------------------
	
	/**
	 * Ein Array, welches fuer jede Taste speichert, ob sie gedrueckt ist, oder
	 * nicht
	 */
	boolean[] keys;
	
	// -----Konstruktor---------------------------------------------------------
	
	/**
	 * Erstellt eine Tastatur mit 512 Plaetzen, die entsprechende Tasten
	 * repraesentieren
	 */
	public Keyboard()
	{
		keys = new boolean[512];
	}
	
	// -----Methoden------------------------------------------------------------
	
	@Override
	public void keyPressed(KeyEvent k)
	{
		keys[k.getKeyCode()] = true;
	}
	
	@Override
	public void keyReleased(KeyEvent k)
	{
		keys[k.getKeyCode()] = false;
	}
	
	@Override
	public void keyTyped(KeyEvent k)
	{
		// Wird ignoriert
	}
	
	/**
	 * Gibt zurueck, ob eine beliebige Taste gedrueckt ist
	 * 
	 * @param index
	 */
	public boolean isPressed(int index)
	{
		if (index <= keys.length)
		{
			return keys[index];
		}
		else
		{
			return false;
		}
	}
}
