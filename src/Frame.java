import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 * 
 * @author Tobias Schleifstein
 * 
 */
@SuppressWarnings("serial")
public class Frame extends JFrame
{
	// -------Instanzvariablen--------------------------------------------------
	
	/** Zeichenebene */
	private DrawArea draw;
	
	/** Gerendertes Bild */
	private BufferedImage image;
	
	// -------Konstruktor-------------------------------------------------------
	
	/**
	 * Erstellt ein Fenster, in welchem das von Game gerenderte Bild angezeigt
	 * wird.
	 * 
	 * @param keyboard
	 *            Schnittstelle zur Tastatur.
	 */
	public Frame(Keyboard keyboard)
	{
		super("LOADING...");
		
		// Fenster einstellen
		setSize(1280, 720);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setVisible(true);
		
		// Tastatur initialisiern
		addKeyListener(keyboard);
		
		// DrawArea erstellen
		draw = new DrawArea();
		draw.setBounds(0, 0, getContentPane().getWidth(), getContentPane()
				.getHeight());
		add(draw);
		
		// Bild erstellen
		image = new BufferedImage(1280, 720, BufferedImage.TYPE_INT_RGB);
	}
	
	// -------Methoden----------------------------------------------------------
	
	/**
	 * Gibt das Bild zurueck, welches gerendert werden soll.
	 * 
	 * @return Bild, welches gerendert werden soll.
	 */
	public BufferedImage getImage()
	{
		return image;
	}
	
	// -------Private Klassen---------------------------------------------------
	
	/**
	 * 
	 * @author Tobias Schleifstein
	 * 
	 */
	private class DrawArea extends JPanel
	{
		@Override
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			
			// Zeichnet das Bild
			g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		}
	}
}
