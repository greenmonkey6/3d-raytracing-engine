/**
 * 
 * @author Tobias Schleifstein
 * 
 */
public class Surface extends Element
{
	// -------Instanzvariablen--------------------------------------------------
	
	/** Ortsvektor der Flaeche. */
	private Point3D start;
	
	/** Richtungsvektoren der Flaeche. */
	private Point3D direction1, direction2;
	
	/** Breite und Hoehe, in welcher die Flaeche definiert ist. */
	private double width, height;
	
	/** Farbe der Flaeche. */
	private int color;
	
	// -------Konstruktor-------------------------------------------------------
	
	/**
	 * Erstellt eine Ebene mit einem Ortsvektor, zwei Richtungsvektoren und
	 * einem Definitionsbereich in einer bestimmten Farbe
	 * 
	 * @param startX
	 *            x-Koordinate des Ortsvektors
	 * @param startY
	 *            y-Koordinate des Ortsvektors
	 * @param startZ
	 *            z-Koordinate des Ortsvektors
	 * @param direction1X
	 *            x-Koordinate des ersten Richtungsvektors
	 * @param direction1Y
	 *            y-Koordinate des ersten Richtungsvektors
	 * @param direction1Z
	 *            z-Koordinate des ersten Richtungsvektors
	 * @param direction2X
	 *            x-Koordinate des zweiten Richtungsvektors
	 * @param direction2Y
	 *            y-Koordinate des zweiten Richtungsvektors
	 * @param direction2Z
	 *            z-Koordinate des zweiten Richtungsvektors
	 * @param width
	 *            Breite der Flaeche
	 * @param height
	 *            Hoehe der Flaeche
	 * @param color
	 *            Farbe der Flaeche
	 */
	public Surface(Point3D start, Point3D direction1, Point3D direction2,
			double width, double height, int color)
	{
		this.start = start;
		
		double direction1Length = Math.sqrt(direction1.x * direction1.x
				+ direction1.y * direction1.y + direction1.z * direction1.z);
		this.direction1 = new Point3D(direction1.x / direction1Length,
				direction1.y / direction1Length, direction1.z
						/ direction1Length);
		
		double direction2Length = Math.sqrt(direction2.x * direction2.x
				+ direction2.y * direction2.y + direction2.z * direction2.z);
		this.direction2 = new Point3D(direction2.x / direction2Length,
				direction2.y / direction2Length, direction2.z
						/ direction2Length);
		
		this.width = width;
		this.height = height;
		
		this.color = color;
	}
	
	// -------Methoden----------------------------------------------------------
	
	@Override
	public void collide(Ray ray)
	{
		// Strahl
		double x1 = ray.getStart().x;
		double y1 = ray.getStart().y;
		double z1 = ray.getStart().z;
		
		double a;
		
		double x2 = ray.getDirection().x;
		double y2 = ray.getDirection().y;
		double z2 = ray.getDirection().z;
		
		// Flaeche
		double x3 = start.x;
		double y3 = start.y;
		double z3 = start.z;
		
		double b;
		
		double x4 = direction1.x;
		double y4 = direction1.y;
		double z4 = direction1.z;
		
		double c;
		
		double x5 = direction2.x;
		double y5 = direction2.y;
		double z5 = direction2.z;
		
		// Berechnung (siehe misc/a berechnen.txt und misc/Gerade mit
		// Flaeche.odf)
		a = ((((x3 - x1) * y4 - (x4 * y3) + x4 * y1) * z5
				+ ((x1 - x3) * y5 + x5 * y3 - (x5 * y1)) * z4
				+ (x4 * y5 - (x5 * y4)) * z3 + (x5 * y4 - (x4 * y5)) * z1) / ((x2
				* y4 - (x4 * y2))
				* z5 + (x5 * y2 - (x2 * y5)) * z4 + (x4 * y5 - (x5 * y4)) * z2));
		
		c = (y3 - y1 - a * y2 + (x3 - x1 - a * x2) / (-x4) * y4)
				/ (x5 / (-x4) * y4 - y5);
		
		b = (x3 - x1 - a * x2 + c * x5) / (-x4);
		
		// ueberpruefen, ob der Strahl die Flaeche im definierten Intervall
		// schneidet
		if (b <= width && b >= -width && c <= height && c >= -height && a > 0)
		{
			
			// Schnittpunkt berechnen
			double x = ray.getStart().x + a * ray.getDirection().x;
			double y = ray.getStart().y + a * ray.getDirection().y;
			double z = ray.getStart().z + a * ray.getDirection().z;
			
			// Entfernung (Betrag) berechnen
			double distance = Math.sqrt(Math.pow(ray.getStart().x - x, 2)
					+ Math.pow(ray.getStart().y - y, 2)
					+ Math.pow(ray.getStart().z - z, 2));
			
			// Guckt, ob sich die Kollision im Richtungsvektor befindet
			boolean withinLightDistance;
			
			if (a <= 1)
			{
				withinLightDistance = true;
			}
			else
			{
				withinLightDistance = false;
			}
			
			// Kollision hinzufuegen
			ray.addCollision(distance, color, new Point3D(x, y, z),
					withinLightDistance);
		}
	}
}
